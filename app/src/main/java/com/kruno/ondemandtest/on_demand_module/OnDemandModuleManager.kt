package com.kruno.ondemandtest.on_demand_module

import android.app.AlertDialog
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import com.google.android.play.core.splitinstall.*
import com.google.android.play.core.splitinstall.model.SplitInstallSessionStatus
import com.kruno.ondemandtest.R

class OnDemandModuleManager(var context: Context) {

    private var manager: SplitInstallManager? = null
    private var listener: SplitInstallStateUpdatedListener? = null
    var maxDownloadSize: Int = 0
    var bytesDownloaded: Int = 0

    /**
     * Constants related only to modules
     * Module name - string defined in module manifest "title"
     * Module path - module package
     * Destination path - full path to specific module destination "module path + class name"
     * */
    companion object {
        private const val SUPPORT_MODULE_PATH = "com.kruno.support"
        const val SUPPORT_MODULE = "support"
        const val SUPPORT_MODULE_SUPPORT_ACTIVITY = "$SUPPORT_MODULE_PATH.SupportActivity"
    }

    init {
        manager = SplitInstallManagerFactory.create(context)
    }

    fun isModuleInstalled(module: String): Boolean {
        manager?.let {
            return it.installedModules.contains(module)
        }

        return false
    }

    /**Before calling navigateToModuleActivity function we need to make sure
     * that module is installed, otherwise Application will crash
     * **/
    fun navigateToModuleActivity(activityPath: String) {
        context.startActivity(Intent().setClassName(context.packageName, activityPath))
    }

    /**
     * In order to provide feedback to user we need to register listener,
     * when finished or failed we need to unregister lister
     * Listener can be registered/unregistered in Activity onResume/onPause as well
     * */
    fun downloadModule(module: String, callback: (Int) -> Unit) {
        manager?.let {
            if (isModuleInstalled(module)) {
                callback(SplitInstallSessionStatus.INSTALLED)
            } else {
                listener = SplitInstallStateUpdatedListener { state ->

                    maxDownloadSize = state.totalBytesToDownload().toInt()
                    bytesDownloaded = state.bytesDownloaded().toInt()

                    when (state.status()) {
                        SplitInstallSessionStatus.INSTALLED -> {
                            it.unregisterListener(listener)
                        }

                        SplitInstallSessionStatus.FAILED -> {
                            it.unregisterListener(listener)
                        }
                    }

                    callback(state.status())
                }

                it.registerListener(listener)

                val request = SplitInstallRequest.newBuilder()
                    .addModule(module)
                    .build()

                it.startInstall(request)
                    .addOnFailureListener {
                        manager?.unregisterListener(listener)
                        callback(SplitInstallSessionStatus.FAILED)
                    }
            }
        }
    }

    /**
     * We use Progress dialog as it blocks UI
     * We can consider adding Progress bar with extra logic to
     * prevent user from double tap and download module again while current
     * download is in progress
     * */
    fun getProgressDialog(): ProgressDialog {
        val progressDialog = ProgressDialog(context)
        progressDialog.setCancelable(false)
        progressDialog.setMessage(context.getString(R.string.on_demand_progress_dialog))
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL)
        progressDialog.progress = 0
        return progressDialog
    }

    fun getAlerDialog(): AlertDialog {
        var builder = AlertDialog.Builder(context)
        builder.setCancelable(true)
        builder.setView(R.layout.layout_loading_dialog)
        var dialog = builder.create()
        return dialog
    }
}