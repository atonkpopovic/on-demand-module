package com.kruno.ondemandtest.on_demand_module

import android.content.Context
import android.support.v7.app.AppCompatActivity
import com.google.android.play.core.splitcompat.SplitCompat

/**
 * Each module activity need to extend
 * */
abstract class BaseSplitActivity : AppCompatActivity(){

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        SplitCompat.install(this)
    }
}