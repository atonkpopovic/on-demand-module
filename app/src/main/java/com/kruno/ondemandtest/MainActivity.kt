package com.kruno.ondemandtest

import android.os.Bundle
import com.kruno.ondemandtest.on_demand_module.BaseSplitActivity
import com.kruno.ondemandtest.util.SupportUtil
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseSplitActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // display version code and packagename for easier debuging
        tvAppVersion.text = "Version: ${BuildConfig.VERSION_CODE} \nPackage: ${BuildConfig.APPLICATION_ID} \nApplication: ${application.packageName}"

        btnLoad.setOnClickListener {
            SupportUtil.loadModule(this)
        }
    }
}
