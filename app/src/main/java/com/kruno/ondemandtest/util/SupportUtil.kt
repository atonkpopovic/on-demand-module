package com.kruno.ondemandtest.util

import android.content.Context
import android.content.Intent
import android.util.Log
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.play.core.splitinstall.model.SplitInstallSessionStatus
import com.kruno.ondemandtest.R
import com.kruno.ondemandtest.on_demand_module.OnDemandModuleManager
import kotlinx.android.synthetic.main.layout_loading_dialog.view.*

/**
 * Simulated object class that starts module download
 * */
object SupportUtil {

    fun loadModule(context: Context) {

        //check if module is installed before navigating to module activity
        val onDemandModuleManager = OnDemandModuleManager(context)
        /*if (onDemandModuleManager.isModuleInstalled(OnDemandModuleManager.SUPPORT_MODULE)){
            onDemandModuleManager.navigateToModuleActivity(OnDemandModuleManager.SUPPORT_MODULE_SUPPORT_ACTIVITY)
            return
        }*/

        //We use Progress dialog to block user UI and prevent user from starting another download
        //while current one is in progress
        //we can consider Progress as per Google guidelines Progress dialog is deprecated
        var progressDialog = onDemandModuleManager.getAlerDialog()
        progressDialog.show()

        var message: TextView = progressDialog.findViewById(R.id.tvMessage)
        var progressBar: ProgressBar = progressDialog.findViewById(R.id.progressBar)
        var tvPercentage: TextView = progressDialog.findViewById(R.id.tvPercentage)
        var tvDownloaded: TextView = progressDialog.findViewById(R.id.tvDownloaded)

        //Starting download and install process
        //Listening to process status and sharing feedback to user
        onDemandModuleManager.downloadModule(OnDemandModuleManager.SUPPORT_MODULE, callback = {
            when (it) {
                SplitInstallSessionStatus.DOWNLOADING -> {
                    message.text = "Downloading"
                    progressBar.max = onDemandModuleManager.maxDownloadSize
                    progressBar.progress = onDemandModuleManager.bytesDownloaded
                    var percentage = (onDemandModuleManager.bytesDownloaded/onDemandModuleManager.maxDownloadSize) * 100
                    tvPercentage.text = "$percentage %"
                    tvDownloaded.text = "${onDemandModuleManager.bytesDownloaded} / ${onDemandModuleManager.maxDownloadSize}"
                }

                SplitInstallSessionStatus.INSTALLED -> {
                    message.text = "Installed"
                    progressDialog.dismiss()

                    onDemandModuleManager.navigateToModuleActivity(OnDemandModuleManager.SUPPORT_MODULE_SUPPORT_ACTIVITY)
                }

                SplitInstallSessionStatus.INSTALLING -> {
                    message.text = "Installing"
                }

                SplitInstallSessionStatus.FAILED -> {
                    message.text = "Fail to download"
                    progressDialog.dismiss()
                }
            }
        })
    }
}