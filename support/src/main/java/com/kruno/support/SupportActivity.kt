package com.kruno.support

import android.os.Bundle
import com.kruno.ondemandtest.on_demand_module.BaseSplitActivity

class SupportActivity : BaseSplitActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_support)
    }
}
