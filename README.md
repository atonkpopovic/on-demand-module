# On demand module example
(Dynamic feature modules)

Prerequisite

- Android Studio v3.3+
- A Google play console account 

Steps to configure

- Add Play Core library
- Add support for Split Install API 
- Create dinamic feature module or
- Convert existing library to be an on-demand module (apply dynamic feature plugin to module, add dependency to app module, add module name to strings)
- Add dynamicFeatures = [ ':module name'] to app module
- Load the Application to Google Play

Important

- While designing classes and logic keep in mind that on demand module depend on :app module, unlike classic Android/Java library module.
- Always check if module is installed before accessing any of its classes or content. Accessing class or content if required module is not installed will cause "class not found exception" and crash the Application because we set explicit path to the content.